
#include "cuda_runtime.h"
#include <cuda_runtime_api.h>
#include <cuda.h>
#include "device_launch_parameters.h"
#include <stdio.h>
#include <math.h>
#include <cooperative_groups.h>
#include <iostream>
#include <fstream>


namespace cg = cooperative_groups;
using namespace std;

const unsigned char solution_mode = 1;
const unsigned int s = 256;
const unsigned int array_size = (s * (s + 1) / 2);
const unsigned short grid_size = 576;
unsigned int padded_array_size; // = array_size + (grid_size - array_size % grid_size);


unsigned char K = 0;
float C1 = 0;
unsigned int steps = 0;

float C = 0;
float C2 = 0;
float C3 = 0;
float stepsize = 0;

unsigned char s_K, unsigned int s_size, unsigned int s_steps, float s_C1, float s_C2, float s_C3, float s_stepsize, float s_C;


int main();

cudaError_t cudaInitializer(float *P, float *Q, float *Pdt, float *Qdt, int *Rshift, int *Lshift, int *Ushift, int *Dshift, int *Rborder, int *Rinside);



__global__ void
__launch_bounds__(grid_size,2)
solveKernel_GridSync(float *P, float *Q, float *Pdt, float *Qdt,int *Rshift, int *Lshift, int *Ushift, int *Dshift, int *Rborder, int *Rinside, unsigned int* d_arraysize, unsigned char *d_K, unsigned int *d_steps, float *d_C, float *d_C1, float *d_C2, float *d_C3, float *d_stepsize) {

	
	register unsigned short thread_ID = threadIdx.x + blockDim.x * blockIdx.x;
	register unsigned int numThreads = gridDim.x*blockDim.x;
	register unsigned int ti = thread_ID;


	register unsigned int R0 = 0;
	register unsigned int R1 = 0;
	register unsigned int R2 = 0;
	register unsigned int R3 = 0;

	register float R4 = 0.0;
	register float R5 = 0.0;
	register float R6 = 0.0;
	
	register float s_C = *d_C;
	register float s_C1 = *d_C1;
	register float s_C2 = *d_C2;
	register float s_C3 = *d_C3;
	
	register unsigned char s_K = *d_K;
	register unsigned int s_size = *d_arraysize;
	register unsigned int s_steps = *d_steps;
	register float s_stepsize = *d_stepsize;

	

	//register float R7 = 0.0;
	//register float R8 = 0.0;
	//register float R9 = 0.0;

	register cg::grid_group g = cg::this_grid();
	
	#pragma unroll
	for (unsigned int j = 0; j < s_steps; j++) {
		#pragma unroll
		for (ti = thread_ID; ti < s_size; ti +=numThreads)
		{

			P[ti] += s_stepsize * Pdt[ti];
			Q[ti] += s_stepsize * Qdt[ti];

		}
		g.sync();

		ti = thread_ID;
		R0 = Rinside[ti];
		R1 = Rborder[ti];
		P[R1] = P[R0];
		Q[R1] = Q[R0];
		

		g.sync();
		#pragma unroll
		for (ti = thread_ID; ti < s_size; ti += numThreads) {

				//Reduce memory accesses by loading the variables once.

				/*

				R0 = Ushift[ti];
				R1 = Dshift[ti];
				R2 = Rshift[ti];
				R3 = Lshift[ti];
				R4 = P[ti];
				R5 = Q[ti];
				R6 =  R4*R4 * R5;

				R7 = P[R2] + P[R3];
				R8 = P[R1] + P[R0];
				R9 = R7 + R8;
				R7 = s_C2 * R9;
				R8 = R7 + R6; // P2Q + s_C2 *(P[right] + P[left] + P[down] + P[up])
				R7 = s_C + R8; //  s_C2 *(P[right] + P[left] + P[down] + P[up]) + P2Q + s_C
				R8 = s_C1 * R4; // s_C1 * P[ti]
				R9 = R7 - R8; //  s_C2 * (P[right] + P[left] + P[down] + P[up]) + P2Q + s_C - s_C1 * P[ti]


				Pdt[ti] = R9;

				R7 = s_K * R4; // s_K * P[ti]
				R8 = R7 - R6; //  s_K * P[ti] - P2Q
				R6 = Q[R2] + Q[R3]; // Q[right] + Q[left]
				R7 = Q[R1] + Q[R0]; // Q[down] + Q[up]
				R9 = R6 + R7; // Q[right] + Q[left] + Q[down] + Q[up]
				R6 = 4 * R5; // 4 * Q[ti] 
				R7 = R9 - R6; // Q[right] + Q[left] + Q[down] + Q[up] - 4 * Q[ti] 
				R6 = s_C3 * R7; // s_C3 * ( Q[right] + Q[left] + Q[down] + Q[up] - 4 * Q[ti]  )
				R7 = R6 + R8; // s_C3 * ( Q[right] + Q[left] + Q[down] + Q[up] - 4 * Q[ti]  ) + s_K * P[ti] - P2Q

				Qdt[ti] = R7;
				*/
				
				R0 = Ushift[ti];
				R1 = Dshift[ti];
				R2 = Rshift[ti];
				R3 = Lshift[ti];
				R4 = P[ti];
				R5 = Q[ti];
				R6 = R4*R4 * R5;


				Pdt[ti] = s_C2 * (P[R2] + P[R3] + P[R1] + P[R0]) + R6 + s_C - s_C1 * R4;
				Qdt[ti] = s_C3 * (Q[R2] + Q[R3] - 4 * R5 + Q[R1] + Q[R0]) - R6 + s_K * R4;
				
		}
		g.sync();
	}
}



__global__ void pdePart1(float *P, float *Q, float *Pdt, float *Qdt, float *d_stepsize, unsigned int *d_arraysize) {
	
	register unsigned int ti = threadIdx.x + blockDim.x * blockIdx.x;
	register unsigned int numThreads = gridDim.x*blockDim.x;


	register float s_stepsize = *d_stepsize;
	register unsigned int s_size = *d_arraysize;

	#pragma unroll
	for (ti; ti < s_size; ti += numThreads)
	{
		

		P[ti] += s_stepsize * Pdt[ti];
		Q[ti] += s_stepsize * Qdt[ti];
		
	}
}

__global__ void pdePart2(float *P, float *Q, int *Rborder, int *Rinside) {
	register unsigned int ti = threadIdx.x + blockDim.x * blockIdx.x;
	register unsigned int rightIn = Rinside[ti];
	register unsigned int rightBorder = Rborder[ti];

	P[rightBorder] = P[rightIn];
	Q[rightBorder] = Q[rightIn];

}

__global__ void pdePart3(float *P, float *Q, float *Pdt, float *Qdt, int *Rshift, int *Lshift, int *Ushift, int *Dshift, unsigned int *d_size, unsigned char *d_K, float *d_C, float *d_C1, float *d_C2, float *d_C3) {
	register unsigned int ti = threadIdx.x + blockDim.x * blockIdx.x;
	register unsigned int numThreads = gridDim.x*blockDim.x;

	register unsigned int s_size = *d_size;
	register unsigned char s_K = *d_K;
	register float s_C = *d_C;
	register float s_C1 = *d_C1;
	register float s_C2 = *d_C2;
	register float s_C3 = *d_C3;



	
	register float P2Q = 0.0;
	register float temp1 = 0.0;
	register float temp2 = 0.0;
	register unsigned int down = 0;
	register unsigned int up = 0;
	register unsigned int left = 0;
	register unsigned int right = 0;
	
	#pragma unroll
	for (ti; ti < s_size; ti += numThreads) {
		//Reduce memory accesses by loading the variables once.
		
		up = Ushift[ti];
		down = Dshift[ti];
		right = Rshift[ti];
		left = Lshift[ti];
		temp1 = P[ti];
		temp2 = Q[ti];
		P2Q = temp1*temp1 * temp2;
		
		Pdt[ti] = s_C2 * (P[right] + P[left] + P[down] + P[up]) + P2Q + s_C - s_C1 * temp1;
		Qdt[ti] = s_C3 * (Q[right] + Q[left] - 4 * temp2 + Q[down] + Q[up]) - P2Q + s_K * temp1;
		
		
		
		/*
		Pdt[ti] = s_C2 * (P[Rshift[ti]] + P[Lshift[ti]] + P[Dshift[ti]] + P[Ushift[ti]]) + P[ti]*P[ti]*Q[ti] + s_C - s_C1 * P[ti];
		Qdt[ti] = s_C3 * (Q[Rshift[ti]] + Q[Lshift[ti]] - 4 * Q[ti] + Q[Dshift[ti]] + Q[Ushift[ti]]) - P[ti]*P[ti]*Q[ti] + s_K * P[ti];
		*/
	}
}

int main()
{

	
	C = 4.5;
	K = 11;
	
	if (solution_mode == 0) {
		
		padded_array_size = array_size + (1024 - array_size % 1024);
	}
	else
	{
		padded_array_size = array_size + (grid_size - array_size % grid_size);
	}
	if (padded_array_size < 1024 * 18) {
		padded_array_size = 1024 * 18;
	}




	float float_s = (float)s;
	float time = 200;
	stepsize = 0.02212 *  pow((float_s - 1) / 20, -1.862) - 6.079e-6;
	printf("Stepsize is %f \n", stepsize);
	steps = time / stepsize;
	float dx = (float)(20.0 / (s - 1.0)) * (20.0 / (s - 1.0));
	
	C2 = 1.0 / dx;
	C3 = 8.0 / dx;
	C1 = K + 1 + C2 * 4;

	printf("%f \n", C);
	printf("%f \n", C1);
	printf("%f \n", C2);
	printf("%f \n", C3);

	printf("Number of steps: %d \n", steps);
	printf("Array Size %d \n", array_size);
	printf("Padded Array Size %d \n", padded_array_size);

	int *left = new int[padded_array_size];
	int *right = new int[padded_array_size];
	int *upper = new int[padded_array_size];
	int *lower = new int[padded_array_size];
	int *right_border = new int[padded_array_size];
	int *right_inside = new int[padded_array_size];

	float *init_p = new float[padded_array_size];
	float *init_q = new float[padded_array_size];
	float *init_pdt = new float[padded_array_size];
	float *init_qdt = new float[padded_array_size];

	int half_index = s / 2;

	printf("Intializing Arrays 1 \n");

	//Initialize p and q vectors
	for (int i = 0; i < padded_array_size; i++) {
		init_p[i] = 0.0;
		init_q[i] = 0.0;
		init_pdt[i] = 0.0;
		init_qdt[i] = 0.0;
	}


	for (int i = array_size; i < padded_array_size; i++) {
		upper[i] = i;
		lower[i] = i;
		left[i] = i;
		right[i] = i;
	}

	for (int i = s; i < padded_array_size; i++) {
		right_inside[i] = padded_array_size-1;
		right_border[i] = padded_array_size-1;
	}

	


	for (int i = 0; i < s; i++) {
		int l_start = ((s * (s + 1) - (s - i) * (s - i + 1)) / 2);
		int end_index = max(-1, half_index - i);
		for (int j = l_start; j < end_index + l_start + 1; j++) {
			init_p[j] = C + 0.1;
			init_q[j] = (K / C) + 0.2;

		}
	}


	//Initialize neumann border look-up tables
	for (int i = 0; i < s; i++) {
		right_border[i] = ((s * (s + 1) - (s - i - 1) * (s - i)) / 2) - 1;
		right_inside[i] = ((s * (s + 1) - (s - i - 1) * (s - i)) / 2) - 2;
	}

	//Initialize laplace index look-up tables
	for (int i = 0; i<s; i++)
	{
		int l_start = ((s * (s + 1) - (s - i) * (s - i + 1)) / 2);
		int l_end = ((s * (s + 1) - (s - i - 1) * (s - i - 1 + 1)) / 2);

		if (1 - i > 0) {
			upper[0] = 1;
			for (int j = 1; j < l_end; j++) {
				upper[j] = j + s - 1;
			}
		}
		else {
			for (int j = l_start; j < l_end; j++) {
				upper[j] = j - s + 1 + (i - 1) * 1;

			}
		}

		lower[l_start] = l_start + 1;
		int k = 0;
		for (int j = l_start + 1; j < l_end; j++) {
			lower[j] = l_end + k;
			k++;
		}

		right[l_end - 1] = l_end - 2;
		for (int j = l_start; j < l_end - 1; j++) {
			right[j] = j + 1;
		}

		left[l_start] = ((s * (s + 1) - (s - i + 1) * (s - i + 2)) / 2) + 1;
		left[0] = 1;

		for (int j = l_start + 1; j < l_end; j++) {
			left[j] = j - 1;
		}
	}

	printf("Finished Initializing Arrays \n");




	// Perform memory allocation and initialize kernels.
	cudaError_t cudaStatus = cudaInitializer(init_p, init_q, init_pdt, init_qdt, right, left, upper, lower, right_border, right_inside);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "addWithCuda failed!");
		return 1;
	}

	ofstream file1 ("C:/Users/Jonas/Documents/Scientific Computing/P.txt");
	ofstream file2 ("C:/Users/Jonas/Documents/Scientific Computing/Q.txt");
	for (int i = 0; i < array_size; i++)
	{
		file1 << init_p[i] << "\n"; 
		file2 << init_q[i] << "\n";
	}
	file1.close();
	file2.close();
	

	cudaStatus = cudaDeviceReset();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceReset failed!");
		return 1;
	}

	return 0;
}





// Allocate memory and initialize kernels.
cudaError_t cudaInitializer(float *P, float *Q, float *Pdt, float *Qdt, int *Rshift, int *Lshift, int *Ushift, int *Dshift, int *Rborder, int *Rinside){

	// Initialize device pointers
	float *d_P;
	float *d_Q;
	float *d_Pdt;
	float *d_Qdt;
	int *d_Rshift;
	int *d_Lshift;
	int *d_Ushift;
	int *d_Dshift;
	int *d_Rborder;
	int *d_Rinside;


	unsigned short *d_s;
	unsigned int *d_size;
	unsigned char *d_K;
	
	unsigned int *d_steps;

	float *d_C;
	float *d_C1;
	float *d_C2;
	float *d_C3;
	float *d_stepsize;

	


	int BLOCKS; //= 18;
	int THREADS; //= 1024;
	int dev = 0;
	int supportsCoopLaunch = 0;
	
	int nDevices;

	printf("Bytes required to store arrays %d \n", array_size * 4 * 4 + s*4*2);



	cudaError_t cudaStatus;
	// Choose which GPU to run on, change this on a multi-GPU system.
	printf("Setting Device \n");
	cudaStatus = cudaSetDevice(0);
	
	



	printf("Copying constants into constant device memory \n");
	// Copy to constant memory


	/*
//	cudaStatus = cudaMemcpyToSymbol(s_s, &s, sizeof(short));
	cudaStatus = cudaMemcpyToSymbol(s_size, &array_size, sizeof(int));
	cudaStatus = cudaMemcpyToSymbol(s_K, &K, sizeof(char));
	cudaStatus = cudaMemcpyToSymbol(s_C1, &C1, sizeof(float));
	cudaStatus = cudaMemcpyToSymbol(s_steps, &steps, sizeof(int));
	cudaStatus = cudaMemcpyToSymbol(s_C, &C, sizeof(float));
	cudaStatus = cudaMemcpyToSymbol(s_C2, &C2, sizeof(float));
	cudaStatus = cudaMemcpyToSymbol(s_C3, &C3, sizeof(float));
	cudaStatus = cudaMemcpyToSymbol(s_stepsize, &stepsize, sizeof(float));
	*/

	printf("Allocating memory on device \n");

	// Allocate memory for arrays.
	cudaStatus = cudaMalloc(&d_P, padded_array_size * sizeof(float));
	cudaStatus = cudaMalloc(&d_Q, padded_array_size * sizeof(float));
	cudaStatus = cudaMalloc(&d_Pdt, padded_array_size * sizeof(float));
	cudaStatus = cudaMalloc(&d_Qdt, padded_array_size * sizeof(float));
	cudaStatus = cudaMalloc(&d_Rshift, padded_array_size * sizeof(int));
	cudaStatus = cudaMalloc(&d_Lshift, padded_array_size* sizeof(int));
	cudaStatus = cudaMalloc(&d_Ushift, padded_array_size* sizeof(int));
	cudaStatus = cudaMalloc(&d_Dshift, padded_array_size * sizeof(int));
	cudaStatus = cudaMalloc(&d_Rinside, padded_array_size * sizeof(int));
	cudaStatus = cudaMalloc(&d_Rborder, padded_array_size* sizeof(int));

	// Allocate Variables.
	cudaStatus = cudaMalloc((void**)&d_size, sizeof(int));
	cudaStatus = cudaMalloc((void**)&d_K, sizeof(char));
	cudaStatus = cudaMalloc((void**)&d_C1, sizeof(float));
	cudaStatus = cudaMalloc((void**)&d_steps, sizeof(int));
	cudaStatus = cudaMalloc((void**)&d_C, sizeof(float));
	cudaStatus = cudaMalloc((void**)&d_C2, sizeof(float));
	cudaStatus = cudaMalloc((void**)&d_C3, sizeof(float));
	cudaStatus = cudaMalloc((void**)&d_stepsize, sizeof(float));
	

	printf("Copying host arrays to device \n");

	// Copy host arrays into device memory.
	cudaStatus = cudaMemcpy(d_P, P, padded_array_size* sizeof(float), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_Q, Q, padded_array_size* sizeof(float), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_Pdt, Pdt, padded_array_size * sizeof(float), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_Qdt, Qdt, padded_array_size* sizeof(float), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_Rshift, Rshift, padded_array_size* sizeof(int), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_Lshift, Lshift, padded_array_size* sizeof(int), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_Ushift, Ushift, padded_array_size* sizeof(int), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_Dshift, Dshift, padded_array_size* sizeof(int), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_Rinside, Rinside, padded_array_size* sizeof(int), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_Rborder, Rborder, padded_array_size* sizeof(int), cudaMemcpyHostToDevice);


	// Copy Variables into Device.

	cudaStatus = cudaMemcpy(d_size, &array_size, sizeof(int), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_K, &K, sizeof(char), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_steps, &steps, sizeof(int), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_stepsize, &stepsize, sizeof(float), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_C, &C, sizeof(float), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_C1, &C1, sizeof(float), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_C2, &C2, sizeof(float), cudaMemcpyHostToDevice);
	cudaStatus = cudaMemcpy(d_C3, &C3, sizeof(float), cudaMemcpyHostToDevice);
	



	cudaOccupancyMaxPotentialBlockSize(&BLOCKS, &THREADS, solveKernel_GridSync, 0, 0);


	// Launch a kernel on the GPU with one thread for each element.

	
	
	cudaStatus = cudaDeviceSynchronize();

	

	switch (solution_mode)
	{
	case 0:
		cudaOccupancyMaxPotentialBlockSize(&BLOCKS, &THREADS, pdePart3, 0, 0);
		printf("#Blocks %d \n", BLOCKS);
		printf("#Threads %d \n", THREADS);
		for (int i = 0; i < steps; i++) {
			
			pdePart1 << <18, 1024>> > (d_P, d_Q, d_Pdt, d_Qdt, d_stepsize, d_size);
			cudaDeviceSynchronize();
			pdePart2 << <18, 1024>> > (d_P, d_Q, d_Rborder, d_Rinside);
			cudaDeviceSynchronize();
			
			pdePart3 << <BLOCKS, THREADS>> > (d_P, d_Q, d_Pdt, d_Qdt, d_Rshift, d_Lshift, d_Ushift, d_Dshift, d_size, d_K, d_C, d_C1, d_C2, d_C3);
			cudaDeviceSynchronize();
		}
		
		break;
	default:
		//solveKernel_GridSync(float *P, float *Q, float *Pdt, float *Qdt,int *Rshift, int *Lshift, int *Ushift, int *Dshift, int *Rborder, int *Rinside, unsigned int* d_arraysize, unsigned char *d_K, unsigned int *d_steps, float *d_C, float *d_C1, float *d_C2, float *d_C3, float *d_stepsize) {
		void *kernelArgs[] = { &d_P , (void *)&d_Q, (void *)&d_Pdt, (void *)&d_Qdt, (void *)&d_Rshift, (void *)&d_Lshift, (void *)&d_Ushift, (void *)&d_Dshift, (void *)&d_Rborder, (void *)&d_Rinside, (void *)&d_size, (void *)&d_K, (void *)&d_steps, (void *)&d_C , (void *)&d_C1, (void *)&d_C2, (void *)&d_C3, (void *)&d_stepsize };
		cudaOccupancyMaxPotentialBlockSize(&BLOCKS, &THREADS, solveKernel_GridSync, 0, 0);
		cudaDeviceGetAttribute(&supportsCoopLaunch, cudaDevAttrCooperativeLaunch, dev);
		printf("Is Cooperative Launch Supported: %d\n", supportsCoopLaunch);
		cudaLaunchCooperativeKernel((void*)solveKernel_GridSync, dim3(BLOCKS, 1, 1), dim3(THREADS, 1, 1), kernelArgs);

		cudaOccupancyMaxPotentialBlockSize(&BLOCKS, &THREADS, pdePart3, 0, 0);
		printf("Max grid dimensions %d", THREADS);
		printf("Max grid dimensions %d", BLOCKS);
		break;
	}


	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		goto Error;
	}


	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
		goto Error;
	}

	// Copy output vector from GPU buffer to host memory.
	cudaStatus = cudaMemcpy(P, d_P, array_size * sizeof(float), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Device -> Host");
		goto Error;
	}



	cudaStatus = cudaMemcpy(Q, d_Q, array_size * sizeof(float), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed! Device -> Host");
		goto Error;
	}
	

Error:
	cudaFree(d_P);
	cudaFree(d_Q);
	cudaFree(d_Pdt);
	cudaFree(d_Qdt);
	cudaFree(d_Rshift);
	cudaFree(d_Lshift);
	cudaFree(d_Ushift);
	cudaFree(d_Dshift);
	cudaFree(d_Rborder);
	cudaFree(d_Rinside);
	


	return cudaStatus;
}